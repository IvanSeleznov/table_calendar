// Copyright 2019 Aleksander Woźniak
// SPDX-License-Identifier: Apache-2.0

import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:dotted_border/dotted_border.dart';

import '../customization/calendar_builders.dart';
import '../customization/calendar_style.dart';

class CellContent extends StatelessWidget {
  static const List<double> _dashPattern = const [7, 6];
  static const _ovulationBorder = BorderType.Circle;
  static const _ovulationPadding = EdgeInsets.all(8.0);
  static const _selectedOvulationPadding = EdgeInsets.all(3.0);

  final DateTime day;
  final DateTime focusedDay;
  final dynamic locale;
  final bool isTodayHighlighted;
  final bool isToday;
  final bool isSelected;
  final bool isRangeStart;
  final bool isRangeEnd;
  final bool isWithinRange;
  final bool isOutside;
  final bool isDisabled;
  final bool isHoliday;
  final bool isWeekend;
  final bool isFertility;
  final bool isPeriod;
  final bool isOvulation;
  final CalendarStyle calendarStyle;
  final CalendarBuilders calendarBuilders;

  const CellContent({
    Key? key,
    required this.day,
    required this.focusedDay,
    required this.calendarStyle,
    required this.calendarBuilders,
    required this.isTodayHighlighted,
    required this.isToday,
    required this.isSelected,
    required this.isRangeStart,
    required this.isRangeEnd,
    required this.isWithinRange,
    required this.isOutside,
    required this.isDisabled,
    required this.isHoliday,
    required this.isWeekend,
    required this.isFertility,
    required this.isPeriod,
    required this.isOvulation,
    this.locale,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dowLabel = DateFormat.EEEE(locale).format(day);
    final dayLabel = DateFormat.yMMMMd(locale).format(day);
    final semanticsLabel = '$dowLabel, $dayLabel';

    Widget? cell =
        calendarBuilders.prioritizedBuilder?.call(context, day, focusedDay);

    if (cell != null) {
      return Semantics(
        label: semanticsLabel,
        excludeSemantics: true,
        child: cell,
      );
    }

    final text = '${day.day}';
    final margin = calendarStyle.cellMargin;
    final padding = calendarStyle.cellPadding;
    final alignment = calendarStyle.cellAlignment;
    final duration = const Duration(milliseconds: 250);

    if (isDisabled) {
      cell = calendarBuilders.disabledBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            padding: padding,
            decoration: calendarStyle.disabledDecoration,
            alignment: alignment,
            child: _text(Text(text, style: calendarStyle.disabledTextStyle),
                Alignment.topCenter),
          );
    } else if (isSelected && isOvulation) {
      cell = calendarBuilders.selectedBuilder?.call(context, day, focusedDay) ??
          Container(
            margin: _selectedOvulationPadding,
            padding: _selectedOvulationPadding,
            decoration: calendarStyle.selectedDecoration,
            alignment: alignment,
            child: DottedBorder(
              borderType: _ovulationBorder,
              dashPattern: _dashPattern,
              padding: _ovulationPadding,
              color: calendarStyle.ovulationCircleColor,
              child: _text(Text(text, style: calendarStyle.selectedTextStyle),
                  Alignment.topCenter),
            ),
          );
    } else if (isSelected) {
      cell = calendarBuilders.selectedBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            padding: padding,
            decoration: calendarStyle.selectedDecoration,
            alignment: alignment,
            child: _text(Text(text, style: calendarStyle.selectedTextStyle),
                Alignment.topCenter),
          );
    } else if (isOvulation) {
      cell = calendarBuilders.selectedBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            alignment: alignment,
            child: DottedBorder(
              borderType: _ovulationBorder,
              dashPattern: _dashPattern,
              padding: _ovulationPadding,
              strokeWidth: calendarStyle.ovulationCircleStrokeWidth,
              color: calendarStyle.ovulationCircleColor,
              child: _text(
                Text(text, style: calendarStyle.fertilityTextStyle),
                Alignment.topCenter,
              ),
            ),
          );
    } else if (isRangeStart) {
      cell =
          calendarBuilders.rangeStartBuilder?.call(context, day, focusedDay) ??
              AnimatedContainer(
                duration: duration,
                margin: margin,
                padding: padding,
                decoration: calendarStyle.rangeStartDecoration,
                alignment: alignment,
                child: _text(
                    Text(text, style: calendarStyle.rangeStartTextStyle),
                    Alignment.topCenter),
              );
    } else if (isRangeEnd) {
      cell = calendarBuilders.rangeEndBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            padding: padding,
            decoration: calendarStyle.rangeEndDecoration,
            alignment: alignment,
            child: _text(Text(text, style: calendarStyle.rangeEndTextStyle),
                Alignment.topCenter),
          );
    } else if (isToday && isTodayHighlighted) {
      cell = calendarBuilders.todayBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            padding: padding,
            decoration: calendarStyle.todayDecoration,
            alignment: alignment,
            child: _text(Text(text, style: calendarStyle.todayTextStyle),
                Alignment.topCenter),
          );
    } else if (isHoliday) {
      cell = calendarBuilders.holidayBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            padding: padding,
            decoration: calendarStyle.holidayDecoration,
            alignment: alignment,
            child: _text(Text(text, style: calendarStyle.holidayTextStyle),
                Alignment.topCenter),
          );
    } else if (isWithinRange) {
      cell =
          calendarBuilders.withinRangeBuilder?.call(context, day, focusedDay) ??
              AnimatedContainer(
                duration: duration,
                margin: margin,
                padding: padding,
                decoration: calendarStyle.withinRangeDecoration,
                alignment: alignment,
                child: _text(
                    Text(text, style: calendarStyle.withinRangeTextStyle),
                    Alignment.topCenter),
              );
    } else if (isFertility) {
      cell = calendarBuilders.outsideBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            padding: padding,
            decoration: calendarStyle.defaultDecoration,
            alignment: alignment,
            child: _text(Text(text, style: calendarStyle.fertilityTextStyle),
                Alignment.topCenter),
          );
    } else if (isPeriod) {
      cell = calendarBuilders.outsideBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
            duration: duration,
            margin: margin,
            padding: padding,
            decoration: calendarStyle.defaultDecoration,
            alignment: alignment,
            child: _text(Text(text, style: calendarStyle.periodDaysTextStyle),
                Alignment.topCenter),
          );
    } else {
      cell = calendarBuilders.defaultBuilder?.call(context, day, focusedDay) ??
          AnimatedContainer(
              duration: duration,
              margin: margin,
              padding: padding,
              decoration: isWeekend
                  ? calendarStyle.weekendDecoration
                  : calendarStyle.defaultDecoration,
              alignment: alignment,
              child: _text(
                  Text(
                    text,
                    style: isWeekend
                        ? calendarStyle.weekendTextStyle
                        : calendarStyle.defaultTextStyle,
                  ),
                  Alignment.topCenter));
    }

    return Semantics(
      label: semanticsLabel,
      excludeSemantics: true,
      child: cell,
    );
  }

  Widget _text(Widget text, Alignment alignment) => FittedBox(
        fit: BoxFit.scaleDown,
        alignment: alignment,
        child: text,
      );
}
