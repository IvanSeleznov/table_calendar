// Copyright 2019 Aleksander Woźniak
// SPDX-License-Identifier: Apache-2.0

import 'dart:collection';

import 'package:table_calendar/table_calendar.dart';

/// Example event class.
class DateData {
  final DateTime date;
  final String title;
  final bool periodDays;
  final bool fertilityWindow;
  final bool ovulationDay;
  final bool symptomsAdded;
  final bool sex;

  const DateData(
      {required this.date,
      required this.title,
      this.periodDays = false,
      this.fertilityWindow = false,
      this.ovulationDay = false,
      this.symptomsAdded = false,
      this.sex = false});

  @override
  String toString() => '${date.day} = $title';
}

/// Example events.
///
/// Using a [LinkedHashMap] is highly recommended if you decide to use a map.
final dateData = LinkedHashMap<DateTime, List<DateData>>(
  equals: isSameDay,
  hashCode: getHashCode,
)..addAll(_dateDataSource);

final _dateDataSource = {
  kToday.subtract(Duration(days: 6)): [
    DateData(
        date: kToday.subtract(Duration(days: 6)),
        title: 'Today\'s Data 1',
        periodDays: true),
  ],
  kToday.subtract(Duration(days: 5)): [
    DateData(
        date: kToday.subtract(Duration(days: 5)),
        title: 'Today\'s Data 2',
        periodDays: true),
  ],
  kToday.subtract(Duration(days: 4)): [
    DateData(
        date: kToday.subtract(Duration(days: 4)),
        title: 'Today\'s Data 3',
        periodDays: true),
  ],
  kToday.subtract(Duration(days: 3)): [
    DateData(
        date: kToday.subtract(Duration(days: 3)),
        title: 'Today\'s Data 4',
        periodDays: true),
    DateData(
        date: kToday.subtract(Duration(days: 3)),
        title: 'Today\'s Data 5',
        symptomsAdded: true),
  ],
  kToday.subtract(Duration(days: 2)): [
    DateData(
        date: kToday.subtract(Duration(days: 2)),
        title: 'Today\'s Data 6',
        sex: true),
    DateData(
        date: kToday.subtract(Duration(days: 2)),
        title: 'Today\'s Data 6.5',
        symptomsAdded: true),
    DateData(
        date: kToday.subtract(Duration(days: 2)),
        title: 'Today\'s Data 7',
        symptomsAdded: true),
  ],
  kToday.subtract(Duration(days: 1)): [
    DateData(
        date: kToday.subtract(Duration(days: 1)),
        title: 'Today\'s Data 8',
        sex: true),
    DateData(
        date: kToday.subtract(Duration(days: 1)),
        title: 'Today\'s Data 9',
        symptomsAdded: true),
  ],
  kToday: [
    DateData(date: kToday, title: 'Today\'s Data 10', sex: true),
    DateData(date: kToday, title: 'Today\'s Data 11', symptomsAdded: true),
  ],
};

int getHashCode(DateTime key) {
  return key.day * 1000000 + key.month * 10000 + key.year;
}

/// Returns a list of [DateTime] objects from [first] to [last], inclusive.
List<DateTime> daysInRange(DateTime first, DateTime last) {
  final dayCount = last.difference(first).inDays + 5;
  return List.generate(
    dayCount,
    (index) => DateTime.utc(first.year, first.month, first.day + index),
  );
}

final kToday = DateTime.now();
final kFirstDay = DateTime(kToday.year - 1, kToday.month, kToday.day);
final kLastDay = DateTime(kToday.year + 1, kToday.month, kToday.day);
