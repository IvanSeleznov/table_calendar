import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

import '../utils.dart';

class TableComplexExample extends StatefulWidget {
  final _cellHeight = 50.0;
  final _midPositionCoefficient = 0.66;

  @override
  _TableComplexExampleState createState() => _TableComplexExampleState();
}

class _TableComplexExampleState extends State<TableComplexExample> {
  late final PageController _pageController;
  late final ScrollController _scrollController;
  late final ValueNotifier<List<DateData>> _selectedDaysData;
  final ValueNotifier<DateTime> _focusedDay = ValueNotifier(DateTime.now());
  final Set<DateTime> _selectedDays = LinkedHashSet<DateTime>(
    equals: isSameDay,
    hashCode: getHashCode,
  );
  CalendarFormat _calendarFormat = CalendarFormat.month;
  RangeSelectionMode _rangeSelectionMode = RangeSelectionMode.toggledOff;
  DateTime? _rangeStart;
  DateTime? _rangeEnd;
  late DateTime _firstDay;
  late DateTime _lastDay;
  late DateTime today;

  @override
  void initState() {
    super.initState();
    today = DateTime.now();
    _firstDay = DateTime(today.year - 2, today.month, today.day);
    _lastDay = DateTime(today.year + 1, today.month, today.day);
    _selectedDays.add(_focusedDay.value);
    _selectedDaysData =
        ValueNotifier(_getDataForDays(_getDaysInBetween(_firstDay, _lastDay)));
  }

  @override
  void dispose() {
    _focusedDay.dispose();
    _selectedDaysData.dispose();
    super.dispose();
  }

  bool get canClearSelection =>
      _selectedDays.isNotEmpty || _rangeStart != null || _rangeEnd != null;

  List<DateData> _getDataForDay(DateTime day) {
    return dateData[day] ?? [];
  }

  List<DateData> _getDataForDays(Iterable<DateTime> days) {
    return [
      for (final d in days) ..._getDataForDay(d),
    ];
  }

  List<DateData> _getEventsForRange(DateTime start, DateTime end) {
    final days = daysInRange(start, end);
    return _getDataForDays(days);
  }

  void _onDaySelected(DateTime selectedDay, DateTime focusedDay) {
    setState(() {
      if (_selectedDays.contains(selectedDay)) {
        _selectedDays.remove(selectedDay);
      } else {
        _selectedDays.add(selectedDay);
      }
      _focusedDay.value = focusedDay;
      _rangeStart = null;
      _rangeEnd = null;
      _rangeSelectionMode = RangeSelectionMode.toggledOff;
    });
    _selectedDaysData.value = _getDataForDays(_selectedDays);
  }

  List<DateTime> _getDaysInBetween(DateTime startDate, DateTime endDate) {
    List<DateTime> days = [];
    for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
      days.add(startDate.add(Duration(days: i)));
    }
    return days;
  }

  void _onRangeSelected(DateTime? start, DateTime? end, DateTime focusedDay) {
    setState(() {
      _focusedDay.value = focusedDay;
      _rangeStart = start;
      _rangeEnd = end;
      _selectedDays.clear();
      _rangeSelectionMode = RangeSelectionMode.toggledOn;
    });

    if (start != null && end != null) {
      _selectedDaysData.value = _getEventsForRange(start, end);
    } else if (start != null) {
      _selectedDaysData.value = _getDataForDay(start);
    } else if (end != null) {
      _selectedDaysData.value = _getDataForDay(end);
    }
  }

  AnimatedContainer buildCalendarDayMarker({
    required String text,
    required String icon,
  }) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      width: 52,
      height: 13,
      child: Center(
        child: Text(
          text,
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 10.0,
          ),
        ),
      ),
    );
  }

  int _getMonthsCount(DateTime first, DateTime last) {
    final yearDif = last.year - first.year;
    final monthDif = last.month - first.month;

    return yearDif * 12 + monthDif;
  }

  int _getWeeksCount(DateTime first, DateTime last) {
    return last.difference(_firstDayOfWeek(first)).inDays ~/ 7;
  }

  DateTime _firstDayOfWeek(DateTime week) {
    final daysBefore = _getDaysBefore(week);
    return week.subtract(Duration(days: daysBefore));
  }

  int _getDaysBefore(DateTime firstDay) {
    return (firstDay.weekday + 7 - getWeekdayNumber(StartingDayOfWeek.monday)) %
        7;
  }

  @override
  Widget build(BuildContext context) {
    final _weeksTillToday = _getWeeksCount(_firstDay, DateTime.now());
    final _monthsTillToday = _getMonthsCount(_firstDay, DateTime.now());

    return Scaffold(
      appBar: AppBar(
        title: Text('TableCalendar - Complex'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 24, left: 12, right: 12),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextButton(
                onPressed: () {
                  _scrollController.animateTo(
                    (_weeksTillToday * widget._cellHeight) +
                        (widget._cellHeight * _monthsTillToday) +
                        (MediaQuery.of(context).size.height *
                            widget._midPositionCoefficient),
                    duration: Duration(seconds: 3),
                    curve: Curves.easeInOut,
                  );
                },
                child: Text('To Page One'),
              ),
              TableCalendar<DateData>(
                firstDay: _firstDay,
                lastDay: _lastDay,
                focusedDay: _focusedDay.value,
                currentDay: _focusedDay.value,
                headerVisible: false,
                startingDayOfWeek: StartingDayOfWeek.sunday,
                selectedDayPredicate: (day) => _selectedDays.contains(day),
                scrollDirection: Axis.vertical,
                rangeStartDay: _rangeStart,
                rangeEndDay: _rangeEnd,
                calendarFormat: _calendarFormat,
                rangeSelectionMode: _rangeSelectionMode,
                dayDataLoader: _getDataForDay,
                onDaySelected: _onDaySelected,
                onRangeSelected: _onRangeSelected,
                onCalendarCreated: (controller) => _pageController = controller,
                onMonthlyCalendarCreated: (controller) =>
                    _scrollController = controller,
                onPageChanged: (focusedDay) => _focusedDay.value = focusedDay,
                monthTextStyle:
                    const TextStyle(color: Color(0xff000000), fontSize: 18),
                weekTextStyle: const TextStyle(
                  color: Colors.grey,
                  fontSize: 10,
                  fontWeight: FontWeight.bold,
                ),
                onFormatChanged: (format) {
                  if (_calendarFormat != format) {
                    setState(() => _calendarFormat = format);
                  }
                },
                calendarStyle: CalendarStyle(
                  markersAlignment: Alignment.topCenter,
                  isTodayHighlighted: true,
                  todayTextStyle: const TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  selectedDecoration:
                      const BoxDecoration(color: Colors.transparent),
                  rangeHighlightScale: 0.0,
                  selectedTextStyle: TextStyle(color: Color(0xffFF9494)),
                  rangeStartTextStyle: TextStyle(color: Color(0xffFF9494)),
                  rangeEndTextStyle: TextStyle(color: Color(0xffFF9494)),
                  withinRangeTextStyle: TextStyle(color: Color(0xffFF9494)),
                  rangeStartDecoration:
                      const BoxDecoration(color: Colors.transparent),
                  rangeEndDecoration:
                      const BoxDecoration(color: Colors.transparent),
                ),
                calendarBuilders: CalendarBuilders(
                  singleMarkerBuilder: (context, date, data) {
                    final children = <Widget>[];
                    if (data.periodDays) if (!children.contains(
                        Text('-', style: TextStyle(color: Colors.black)))) {
                      children.add(
                        Text('-', style: TextStyle(color: Colors.black)),
                      );
                    }

                    if (data.sex) if (!children.contains(
                        Text(',', style: TextStyle(color: Colors.black)))) {
                      children.add(
                        Text(',', style: TextStyle(color: Colors.black)),
                      );
                    }

                    if (data.symptomsAdded) if (!children.contains(
                        Text('_', style: TextStyle(color: Colors.black)))) {
                      children.add(
                        Text('_', style: TextStyle(color: Colors.black)),
                      );
                    }

                    if (data.ovulationDay) if (!children.contains(
                        Text('=', style: TextStyle(color: Colors.black)))) {
                      children.add(
                        Text('=', style: TextStyle(color: Colors.black)),
                      );
                    }

                    return Column(
                        mainAxisSize: MainAxisSize.min, children: children);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
